# Open Weather App [2021]

Tried to recreate a Open Weather App in 4 hours.

## Dependencies
- [http](https://pub.dev/packages/http)

## App Overview

### Main Weather Screen

![Weather Screen](/images/readme/weather_screen.png "Weather Screen")

### Search Screen

![Weather Screen](/images/readme/search_screen.png "Search Screen")

### Settings Screen
![Weather Screen](/images/readme/settings_screen.png "Settings Screen")
