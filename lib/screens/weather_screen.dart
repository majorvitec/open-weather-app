import 'package:flutter/material.dart';
import 'package:open_weather_app/screens/search_screen.dart';
import 'package:open_weather_app/screens/settings_screen.dart';
import 'package:open_weather_app/widgets/daily_weather_details_section.dart';
import 'package:open_weather_app/widgets/daily_temperature_section.dart';
import 'package:open_weather_app/widgets/next_days_temperature_prognosis_section.dart';
import 'package:open_weather_app/widgets/next_hours_temperature_prognosis_section.dart';
import 'package:open_weather_app/widgets/precipitation_diagram_section.dart';

class WeatherScreen extends StatelessWidget {
  final AsyncSnapshot snapshot;
  const WeatherScreen({
    Key? key,
    required this.snapshot,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var weatherData = snapshot.data!;

    return Scaffold(
      appBar: AppBar(
        title: GestureDetector(
          child: Row(
            children: [
              const Icon(Icons.search),
              Text(weatherData.city['name']),
            ],
          ),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => SearchScreen(),
              ),
            );
          },
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 16.0),
            child: IconButton(
              icon: const Icon(Icons.settings),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const SettingsScreen(),
                  ),
                );
              },
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: 150,
              child: DailyTemperatureSection(
                temp: weatherData.dailyTemp['temp'],
                description: weatherData.dailyTemp['description'],
                feelsLike: weatherData.dailyTemp['feelsLike'],
              ),
            ),
            const SizedBox(
              height: 150,
              child: PrecipitationDiagramSection(),
            ),
            SizedBox(
              height: 70,
              child: DailyWeatherDetailsSection(
                wind: weatherData.dailyDetails['wind'],
                humidity: weatherData.dailyDetails['humidity'],
                uvIndex: weatherData.dailyDetails['uvIndex'],
                pressure: weatherData.dailyDetails['pressure'],
                visibility: weatherData.dailyDetails['visibility'],
                dewPoint: weatherData.dailyDetails['dewPoint'],
              ),
            ),
            Container(
              height: 70,
              padding: const EdgeInsets.only(left: 8.0, right: 8.0),
              child: NextHoursTemperaturePrognosisSection(),
            ),
            const NextDaysTemperaturePrognosisSection(),
          ],
        ),
      ),
    );
  }
}
