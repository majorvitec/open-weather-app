class Weather {
  final Map<String, dynamic> city;
  final Map<String, dynamic> dailyTemp;
  final Map<String, dynamic> dailyDetails;

  Weather({
    required this.city,
    required this.dailyTemp,
    required this.dailyDetails,
  });

  factory Weather.fromJson(Map<String, dynamic> json) {
    var list = json['list'][0];
    var main = list['main'];
    var weather = list['weather'][0];
    var wind = list['wind'];

    var feelsLike = main['feels_like'];
    if (feelsLike is int) {
      feelsLike = feelsLike.toDouble();
    }

    var city = json['city'];
    var dailyTemp = {
      'temp': main['temp'],
      'feelsLike': feelsLike,
      'description': weather['description']
    };

    var dailyDetails = {
      'wind': wind['speed'],
      'humidity': main['humidity'],
      'uvIndex': main['temp_kf'],
      'pressure': main['pressure'],
      'visibility': list['visibility'],
      'dewPoint': main['temp_max'] - main['temp_min'],
    };

    return Weather(
      city: city,
      dailyTemp: dailyTemp,
      dailyDetails: dailyDetails,
    );
  }
}
