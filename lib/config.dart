import 'package:flutter/material.dart';

/* connection */
const String kApiKey = '79bf0e9a5de681a821b494f9682afc58';

const String kUnitsMetric = 'metric'; // temperature in Celsius
const String kUnitsImperial = 'imperial'; // temperature in Fahrenheit

const String kApiUrl =
    'http://api.openweathermap.org/data/2.5/forecast?id=524901&units=$kUnitsMetric&appid=$kApiKey';

/* Theme */
const kAppColorScheme = ColorScheme(
  brightness: Brightness.light,
  primary: Colors.white,
  onPrimary: Colors.black,
  primaryVariant: Colors.grey,
  secondary: Colors.grey,
  secondaryVariant: Colors.grey,
  onSecondary: Colors.grey,
  background: Colors.grey,
  onBackground: Colors.grey,
  surface: Colors.grey,
  onSurface: Colors.grey,
  error: Colors.grey,
  onError: Colors.grey,
);
