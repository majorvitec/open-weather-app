import 'package:flutter/material.dart';

class DailyWeatherDetailEntry extends StatelessWidget {
  final String name;

  const DailyWeatherDetailEntry({
    Key? key,
    required this.name,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      name,
      style: const TextStyle(
        fontSize: 12.0,
        fontWeight: FontWeight.bold,
      ),
    );
  }
}
