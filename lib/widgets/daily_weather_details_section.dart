import 'package:flutter/material.dart';
import 'package:open_weather_app/widgets/daily_weather_details_entry.dart';

class DailyWeatherDetailsSection extends StatelessWidget {
  final double wind;
  final int humidity;
  final double uvIndex;
  final int pressure;
  final int visibility;
  final double dewPoint;

  const DailyWeatherDetailsSection({
    Key? key,
    required this.wind,
    required this.humidity,
    required this.uvIndex,
    required this.pressure,
    required this.visibility,
    required this.dewPoint,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double calcVisibility = visibility / 1000;

    return Container(
      margin: const EdgeInsets.only(left: 8.0, right: 8.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5.0),
        color: Colors.grey.shade200,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right: 4.0),
                    child:
                        DailyWeatherDetailEntry(name: 'Wind: $wind km/h WSW'),
                  ),
                  const Icon(Icons.ac_unit_sharp, size: 15.0),
                ],
              ),
              DailyWeatherDetailEntry(name: 'Humidity: $humidity %'),
              DailyWeatherDetailEntry(
                name: 'UV index: ${uvIndex.toStringAsFixed(1)}',
              ),
            ],
          ),
          const SizedBox(height: 10.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              DailyWeatherDetailEntry(name: 'Pressure: $pressure hPa'),
              DailyWeatherDetailEntry(name: 'Visibility: $calcVisibility km'),
              DailyWeatherDetailEntry(
                name: 'Dew point: ${dewPoint.toStringAsFixed(0)} °C',
              ),
            ],
          ),
        ],
      ),
    );
  }
}
