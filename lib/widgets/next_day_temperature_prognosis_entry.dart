import 'package:flutter/material.dart';

class NextDayTemperaturePrognosisEntry extends StatelessWidget {
  final String temp;
  final String date;

  const NextDayTemperaturePrognosisEntry({
    Key? key,
    required this.temp,
    required this.date,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 8),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(color: Colors.grey.shade200),
        ),
      ),
      child: ListTile(
        leading: Text(
          date,
          style: const TextStyle(fontSize: 12.0, fontWeight: FontWeight.bold),
        ),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 8.0),
              child: Text(
                temp,
                style: const TextStyle(
                    fontSize: 12.0, fontWeight: FontWeight.bold),
              ),
            ),
            const Icon(
              Icons.ac_unit_sharp,
              size: 15.0,
            ),
          ],
        ),
        trailing: const Icon(
          Icons.arrow_forward_ios_rounded,
          size: 15.0,
        ),
        onTap: () {
          // load details
        },
      ),
    );
  }
}
