import 'package:flutter/material.dart';
import 'package:open_weather_app/helpers/string_helper.dart';

class DailyTemperatureSection extends StatelessWidget {
  final double temp;
  final double feelsLike;
  final String description;

  const DailyTemperatureSection({
    Key? key,
    required this.description,
    required this.temp,
    required this.feelsLike,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: const EdgeInsets.only(right: 10.0),
              child: const Icon(
                Icons.ac_unit,
                size: 40.0,
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  description.capitalize(),
                  style: const TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const Text(
                  'Light air',
                  style: TextStyle(
                    color: Colors.grey,
                    fontWeight: FontWeight.bold,
                  ),
                )
              ],
            )
          ],
        ),
        Text(
          temp.toStringAsFixed(0) + ' °C',
          style: const TextStyle(
            fontSize: 45.0,
            fontWeight: FontWeight.bold,
          ),
        ),
        Text(
          'Feels like ' + feelsLike.toStringAsFixed(0) + ' °C',
          style: const TextStyle(
            fontSize: 12.0,
            color: Colors.grey,
            fontWeight: FontWeight.bold,
          ),
        ),
      ],
    );
  }
}
