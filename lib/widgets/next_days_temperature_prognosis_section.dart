import 'package:flutter/material.dart';
import 'package:open_weather_app/widgets/next_day_temperature_prognosis_entry.dart';

class NextDaysTemperaturePrognosisSection extends StatelessWidget {
  const NextDaysTemperaturePrognosisSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: const [
        NextDayTemperaturePrognosisEntry(temp: '10 / 2 °C', date: 'Fri Nov 05'),
        NextDayTemperaturePrognosisEntry(temp: '11 / 3 °C', date: 'Sat Nov 06'),
        NextDayTemperaturePrognosisEntry(temp: '12 / 4 °C', date: 'Sun Nov 07'),
        NextDayTemperaturePrognosisEntry(temp: '13 / 5 °C', date: 'Mon Nov 08'),
        NextDayTemperaturePrognosisEntry(temp: '14 / 6 °C', date: 'Tuu Nov 09'),
        NextDayTemperaturePrognosisEntry(temp: '15 / 7 °C', date: 'Wed Nov 10'),
        NextDayTemperaturePrognosisEntry(temp: '16 / 8 °C', date: 'Thu Nov 11'),
      ],
    );
  }
}
