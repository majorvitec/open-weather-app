import 'package:flutter/material.dart';
import 'package:open_weather_app/config.dart';
import 'package:open_weather_app/models/weather.dart';
import 'package:open_weather_app/screens/error_screen.dart';
import 'package:open_weather_app/screens/loading_screen.dart';
import 'package:open_weather_app/screens/weather_screen.dart';
import 'package:open_weather_app/services/weather_service.dart';

void main() {
  runApp(const Home());
}

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Open Weather',
      theme: ThemeData(colorScheme: kAppColorScheme),
      home: const HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final WeatherService _weatherService = WeatherService();
  late Future<Weather> _futureWeather;

  @override
  void initState() {
    super.initState();
    _futureWeather = _weatherService.fetchWeather();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Weather>(
      future: _futureWeather,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return WeatherScreen(snapshot: snapshot);
        } else if (snapshot.hasError) {
          return ErrorScreen(snapshot: snapshot);
        }

        return const LoadingScreen();
      },
    );
  }
}
